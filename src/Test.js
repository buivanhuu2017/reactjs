import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

class Test extends Component {

  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id
    }
  }

  componentDidMount() {
    // cho nay gia su lay danh sanh question dua vao this.state.id hoac this.props.match.params.id deu ko dc cap nhat
    // khi bam vao link
    // thuc te no ko goi componentDidMount khi link thay doi, vi component chua unmount nen no ko mount lai
  }

  render() {
    return (
      <div>
        <NavLink to='/test/1'>Test 1</NavLink>
        <NavLink to='/test/2'>Test 2</NavLink>
        <hr />
        <p>{this.props.match.params.id}</p>
      </div>
    );
  }
}

export default Test;
